const colors = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

const renderButton = () => {
  let HTMLcontent = `<button class="color-button pallet active" onclick=changeColor("pallet}")></button>`;
  for (let i = 1; i < colors.length; i++) {
    HTMLcontent += `<button class="color-button ${colors[i]}" onclick=changeColor("${colors[i]}")></button>`;
  }

  document.getElementById("colorContainer").innerHTML = HTMLcontent;
};

renderButton();

const changeColor = (color) => {
  document.getElementById("house").classList.remove(...colors);
  document.getElementById("house").classList.add(color);
};

// Get the container element
var btnContainer = document.getElementById("colorContainer");

// Get all buttons with class="btn" inside the container
var btns = btnContainer.getElementsByClassName("color-button");

// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function () {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}
