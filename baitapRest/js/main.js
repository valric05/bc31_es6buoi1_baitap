const findAvg = (...grades) => {
  if (!grades.length) return 0;
  let sum = 0;
  grades.forEach((grades) => (sum += grades));
  return sum / grades.length;
};

const findAvg1 = () => {
  let toan = document.getElementById("inpToan").value * 1;
  let ly = document.getElementById("inpLy").value * 1;
  let hoa = document.getElementById("inpHoa").value * 1;
  result = findAvg(toan, ly, hoa);
  console.log("result: ", result);
  document.getElementById("tbKhoi1").innerText = result;
};

const findAvg2 = () => {
  let van = document.getElementById("inpVan").value * 1;
  let su = document.getElementById("inpSu").value * 1;
  let dia = document.getElementById("inpDia").value * 1;
  let tiengAnh = document.getElementById("inpEnglish").value * 1;
  result = findAvg(van, su, dia, tiengAnh);
  console.log("result: ", result);
  document.getElementById("tbKhoi2").innerText = result;
};
